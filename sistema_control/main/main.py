import threading
import tkinter as tk
from estacionPintura import EstacionPintura
from estacionSoldadura import EstacionSoldadura
from estacionEnsamble import EstacionEnsamble
from controladorDePiezas import ControladorDePiezas


class LineaEnsamblajeApp:
    def __init__(self):
        self.estacion_pintura = EstacionPintura(500, 5)
        self.estacion_soldadura = EstacionSoldadura(600, self.estacion_pintura)
        self.estacion_ensamble = EstacionEnsamble(50, self.estacion_soldadura)
        self.controlador_de_piezas = ControladorDePiezas(50, self.estacion_ensamble)
        self.root = tk.Tk()
        self.root.title("Control de Línea de Ensamblaje")

        # Etiqueta para mostrar "Controlador de piezas"
        frame_controlador = tk.Frame(self.root)
        frame_controlador.pack(side="left", padx=10)  # Colocar a la izquierda con espacio en el margen derecho

        label_controlador_de_piezas = tk.Label(frame_controlador, text="Controlador de piezas")
        label_controlador_de_piezas.pack()

        # Etiqueta cantidad de piezas controladas
        label_controlador_de_piezas_controladas = tk.Label(
            frame_controlador, text="Piezas controladas " + str(self.controlador_de_piezas.cantidad_piezas_controladas)
        )
        label_controlador_de_piezas_controladas.pack()

        # Etiqueta para indicar el estado controlador de piezas
        canvas_controlador = tk.Canvas(frame_controlador, width=50, height=50)
        canvas_controlador.pack()
        label_estado_controlador = tk.Label(frame_controlador, text=self.controlador_de_piezas.estado)
        label_estado_controlador.pack()

        # Botón para reanudar la línea
        button_reanudar_controlador = tk.Button(frame_controlador, text="Reanudar controlador",
                                            command=self.reiniciar_controlador)
        button_reanudar_controlador.pack()

        # Etiqueta para mostrar "Estación de ensamble"

        frame_ensamble = tk.Frame(self.root)
        frame_ensamble.pack(side="left", padx=10)  # Colocar a la izquierda con espacio en el margen derecho

        label_estacion_ensamble = tk.Label(frame_ensamble, text="Estación de ensamble")
        label_estacion_ensamble.pack()

        # Etiqueta cantidad de piezas ensambladas
        label_estacion_ensamble_cantidad_piezas_ensambladas = tk.Label(
            frame_ensamble, text="Total de piezas ensambladas " + str(self.estacion_ensamble.cantidad_piezas_hechas)
        )
        label_estacion_ensamble_cantidad_piezas_ensambladas.pack()

        # Etiqueta para indicar el estado de la estación de ensamble
        canvas_ensamble = tk.Canvas(frame_ensamble, width=50, height=50)
        canvas_ensamble.pack()
        label_estado_estacion_ensamble = tk.Label(frame_ensamble, text=self.estacion_ensamble.estado)
        label_estado_estacion_ensamble.pack()

        # Botón para reponer lubricante
        button_reponer_lubricante = tk.Button(frame_ensamble, text="Reponer Lubricante", command=self.reponer_lubricante)
        button_reponer_lubricante.pack()

        # Botón para reanudar la línea
        button_reanudar_ensamble = tk.Button(frame_ensamble, text="Reanudar Línea de Ensamblaje",
                                            command=self.reiniciar_linea_ensamble)
        button_reanudar_ensamble.pack()

        # Crear un Frame para la estación de soldadura
        frame_soldadura = tk.Frame(self.root)
        frame_soldadura.pack(side="left", padx=10)  # Colocar a la izquierda con espacio en el margen derecho

        # Etiqueta para mostrar "Estación de soldadura"
        label_estacion_soldadura = tk.Label(frame_soldadura, text="Estación  de soldadura")
        label_estacion_soldadura.pack()

        # Etiqueta cantidad de piezas soldadas
        label_estacion_soldadura_cantidad_piezas_soldadas = tk.Label(
            frame_soldadura, text="Cantidad de piezas disponibles " + str(self.estacion_soldadura.cantidad_piezas_soldadas)
        )
        label_estacion_soldadura_cantidad_piezas_soldadas.pack()

        # Etiqueta para indicar el estado de la estación de soldadura
        canvas_soldadura = tk.Canvas(frame_soldadura, width=50, height=50)
        canvas_soldadura.pack()
        label_estado_estacion_soldadura = tk.Label(frame_soldadura, text=self.estacion_soldadura.estado)
        label_estado_estacion_soldadura.pack()

        # Botón para reponer material soldadura
        button_reponer_material_soldadura = tk.Button(frame_soldadura, text="Reponer Material", command=self.reponer_material_soldadura)
        button_reponer_material_soldadura.pack()

        # Botón para reanudar la línea
        button_reanudar_soldadura = tk.Button(frame_soldadura, text="Reanudar Línea de Ensamblaje",
                                            command=self.reiniciar_linea_soldadura)
        button_reanudar_soldadura.pack()

        # Etiqueta para mostrar "Estación de pintura"

        frame_pintura = tk.Frame(self.root)
        frame_pintura.pack(side="left", padx=10)  # Colocar a la izquierda con espacio en el margen derecho

        label_estacion_pintura = tk.Label(frame_pintura, text="Estación de pintura")
        label_estacion_pintura.pack()

        # Etiqueta cantidad de piezas pintadas
        label_estacion_pintura_cantidad_piezas_pintadas = tk.Label(
            frame_pintura, text="Total de piezas pintadas " + str(self.estacion_pintura.cantidad_piezas_hechas)
        )
        label_estacion_pintura_cantidad_piezas_pintadas.pack()

        # Etiqueta para indicar el estado de la estación de pintura
        canvas_pintura = tk.Canvas(frame_pintura, width=50, height=50)
        canvas_pintura.pack()
        label_estado_estacion_pintura = tk.Label(frame_pintura, text=self.estacion_pintura.estado)
        label_estado_estacion_pintura.pack()


        # Botón para reponer pintura
        button_reponer_pintura = tk.Button(frame_pintura, text="Reponer Pintura", command=self.reponer_pintura)
        button_reponer_pintura.pack()

        # Botón para reanudar la línea
        button_reanudar_pintura = tk.Button(frame_pintura, text="Reanudar Línea de Ensamblaje",
                                            command=self.reiniciar_linea_pintura)
        button_reanudar_pintura.pack()

        #Controlador
        self.label_estado_controlador = label_estado_controlador
        self.label_controlador_de_piezas_controladas = label_controlador_de_piezas_controladas
        self.canvas_controlador = canvas_controlador
        self.button_reanudar_controlador = button_reanudar_controlador

        #Ensamble
        self.label_estado_estacion_ensamble = label_estado_estacion_ensamble
        self.label_estacion_ensamble_cantidad_piezas_ensambladas = label_estacion_ensamble_cantidad_piezas_ensambladas
        self.canvas_ensamble = canvas_ensamble
        self.button_reponer_lubricante = button_reponer_lubricante
        self.button_reanudar_ensamble = button_reanudar_ensamble

        #Soldadura
        self.label_estado_estacion_soldadura = label_estado_estacion_soldadura
        self.label_estacion_soldadura_cantidad_piezas_soldadas = label_estacion_soldadura_cantidad_piezas_soldadas
        self.canvas_soldadura = canvas_soldadura
        self.button_reponer_material_soldadura = button_reponer_material_soldadura
        self.button_reanudar_soldadura = button_reanudar_soldadura

        #Pintura
        self.label_estado_estacion_pintura = label_estado_estacion_pintura
        self.label_estacion_pintura_cantidad_piezas_pintadas = label_estacion_pintura_cantidad_piezas_pintadas
        self.canvas_pintura = canvas_pintura
        self.button_reponer_pintura = button_reponer_pintura
        self.button_reanudar_pintura = button_reanudar_pintura

        self.actualizar_interfaz()

    def actualizar_interfaz(self):
        self.actualizar_estado_estacion_pintura()
        self.actualizar_estado_estacion_soldadura()
        self.actualizar_estado_estacion_ensamblaje()
        self.actualizar_estado_controlador()
        self.actualizar_cantidad_piezas_pintadas()
        self.actualizar_cantidad_piezas_soldadas()
        self.actualizar_cantidad_piezas_ensambladas()
        self.actualizar_cantidad_piezas_controladas()
        self.root.after(1000, self.actualizar_interfaz)

    def actualizar_estado_estacion_pintura(self):
        self.label_estado_estacion_pintura.config(text=self.estacion_pintura.estado)
        self.button_reanudar_pintura.config(state="disabled")
        self.button_reponer_pintura.config(state="disabled")
        if self.estacion_pintura.error:
            self.canvas_pintura.configure(bg="red")
            if not self.estacion_pintura.pintura_disponible:
                self.button_reponer_pintura.config(state="normal")
            else:
                self.button_reanudar_pintura.config(state="normal")
        elif not self.estacion_pintura.tiene_piezas_para_pintar:
            self.canvas_pintura.configure(bg="yellow")
        else:
            self.canvas_pintura.configure(bg="green")

    def actualizar_estado_estacion_soldadura(self):
        self.label_estado_estacion_soldadura.config(text=self.estacion_soldadura.estado)
        self.button_reanudar_soldadura.config(state="disabled")
        self.button_reponer_material_soldadura.config(state="disabled")
        if self.estacion_soldadura.error:
            self.canvas_soldadura.configure(bg="red")
            if not self.estacion_soldadura.material_disponible:
                self.button_reponer_material_soldadura.config(state="normal")
            else:
                self.button_reanudar_soldadura.config(state="normal")
        elif not self.estacion_soldadura.tiene_espacio() or not self.estacion_soldadura.tiene_piezas_para_soldar:
            self.canvas_soldadura.configure(bg="yellow")
        else:
            self.canvas_soldadura.configure(bg="green")

    def actualizar_estado_estacion_ensamblaje(self):
        self.label_estado_estacion_ensamble.config(text=self.estacion_ensamble.estado)
        self.button_reanudar_ensamble.config(state="disabled")
        self.button_reponer_lubricante.config(state="disabled")
        if self.estacion_ensamble.error:
            self.canvas_ensamble.configure(bg="red")
            if not self.estacion_ensamble.lubricante_disponible:
                self.button_reponer_lubricante.config(state="normal")
            else:
                self.button_reanudar_ensamble.config(state="normal")
        elif not self.estacion_ensamble.tiene_espacio() or not self.estacion_ensamble.tiene_piezas_para_ensamblar:
            self.canvas_ensamble.configure(bg="yellow")
        else:
            self.canvas_ensamble.configure(bg="green")

    def actualizar_estado_controlador(self):
        self.label_estado_controlador.config(text=self.controlador_de_piezas.estado)
        self.button_reanudar_controlador.config(state="disabled")
        if self.controlador_de_piezas.error:
            self.canvas_controlador.configure(bg="red")
            self.button_reanudar_controlador.config(state="normal")
        elif not self.controlador_de_piezas.tiene_espacio() or not self.controlador_de_piezas.tiene_piezas_para_controlar():
            self.canvas_controlador.configure(bg="yellow")
        else:
            self.canvas_controlador.configure(bg="green")

    def actualizar_cantidad_piezas_pintadas(self):
        cantidad_piezas = self.estacion_pintura.cantidad_piezas_hechas
        self.label_estacion_pintura_cantidad_piezas_pintadas.config(
            text="Cantidad de piezas pintadas " + str(cantidad_piezas)
        )

    def actualizar_cantidad_piezas_soldadas(self):
        cantidad_piezas = self.estacion_soldadura.cantidad_piezas_soldadas
        self.label_estacion_soldadura_cantidad_piezas_soldadas.config(
            text="Cantidad de piezas soldadas " + str(cantidad_piezas)
        )
    
    def actualizar_cantidad_piezas_ensambladas(self):
        cantidad_piezas = self.estacion_ensamble.cantidad_piezas_hechas
        self.label_estacion_ensamble_cantidad_piezas_ensambladas.config(
            text="Cantidad de piezas ensambladas " + str(cantidad_piezas)
        )

    def actualizar_cantidad_piezas_controladas(self):
        cantidad_piezas = self.controlador_de_piezas.cantidad_piezas_controladas
        self.label_controlador_de_piezas_controladas.config(
            text="Cantidad de piezas controladas " + str(cantidad_piezas)
        )

    def reponer_pintura(self):
        self.estacion_pintura.reponer_pintura()

    def reponer_material_soldadura(self):
        self.estacion_soldadura.reponer_material()

    def reponer_lubricante(self):
        self.estacion_ensamble.reponer_lubricante()

    def reiniciar_linea_pintura(self):
        self.estacion_pintura.reanudar()

    def reiniciar_linea_soldadura(self):
        self.estacion_soldadura.reanudar()

    def reiniciar_linea_ensamble(self):
        self.estacion_ensamble.reanudar()

    def reiniciar_controlador(self):
        self.controlador_de_piezas.reanudar()

    def iniciar_aplicacion(self):
        self.root.mainloop()


if __name__ == "__main__":
    app = LineaEnsamblajeApp()
    t_linea_controlador = threading.Thread(target=app.controlador_de_piezas.encender_linea, args=(app.controlador_de_piezas,))
    t_linea_controlador.start()
    t_linea_ensamble = threading.Thread(target=app.estacion_ensamble.encender_linea, args=(app.estacion_ensamble,))
    t_linea_ensamble.start()
    t_linea_soldadura = threading.Thread(target=app.estacion_soldadura.encender_linea, args=(app.estacion_soldadura,))
    t_linea_soldadura.start()
    t_linea_pintura = threading.Thread(target=app.estacion_pintura.encender_linea, args=(app.estacion_pintura,))
    t_linea_pintura.start()
    app.iniciar_aplicacion()
