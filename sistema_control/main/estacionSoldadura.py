import threading
import random
import time


class EstacionSoldadura:
    def __init__(self, gr_de_material, estacion_pintura):
        self.estacion_pintura = estacion_pintura
        self.cantidad_piezas_soldadas = 0
        self.capacidad = 5
        self.gr_de_material = gr_de_material
        self.error = False
        self.tiene_piezas_para_soldar = False
        self.material_disponible = True
        self.semaforo = threading.Semaphore(1)
        self.cv = threading.Condition()
        self.estado = "Soldando"

    def soldar_pieza(self):
        self.tiene_piezas_para_soldar = True

    def tiene_espacio(self):
        return self.cantidad_piezas_soldadas < self.capacidad

    def detener(self):
        self.semaforo.acquire()
        with self.cv:
            self.cv.notify()

    def reanudar(self):
        self.semaforo.release()
        self.error = False

    def verificar_material(self):
        return self.gr_de_material >= 200

    def reponer_material(self):
        self.gr_de_material = 2000
        self.error = False
        self.material_disponible = True
        self.reanudar()

    def encender_linea(self, estacion_soldadura):
        while True:
            verificar_material = estacion_soldadura.verificar_material()
            tiene_espacio = estacion_soldadura.tiene_espacio()
            tiene_pieza_para_soldar = self.tiene_piezas_para_soldar
            no_hay_error = not self.error
            if verificar_material and tiene_espacio and tiene_pieza_para_soldar and no_hay_error:
                self.estado = "Soldando"
                if random.uniform(0, 1) < 0.10:
                    self.error = True
                else:
                    time.sleep(2)
                    self.cantidad_piezas_soldadas += 1
                    self.gr_de_material -= 200
                    self.pieza_lista_para_pintar()
                    self.tiene_piezas_para_soldar = False
            elif not verificar_material:
                self.estado = "Sin material"
                self.error = True
                self.material_disponible = False
                estacion_soldadura.detener()
            elif not tiene_espacio:
                self.estado = "Capacidad máxima alcanzada"
                self.pieza_lista_para_pintar()
            elif not tiene_pieza_para_soldar:
                self.estado = "Sin piezas para soldar"
            else:
                self.estado = "Hay un error en la línea"
                self.error = True
                estacion_soldadura.detener()

    def pieza_lista_para_pintar(self):
        if not self.estacion_pintura.tiene_piezas_para_pintar:
            self.estacion_pintura.pintar_pieza()
            self.cantidad_piezas_soldadas -= 1
