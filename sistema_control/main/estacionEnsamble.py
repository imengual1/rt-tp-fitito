import threading
import random
import time


class EstacionEnsamble:
    def __init__(self, ml_de_lubricante, estacion_soldadura):
        self.estacion_soldadura = estacion_soldadura
        self.cantidad_piezas_hechas = 0
        self.capacidad = 3
        self.ml_de_lubricante = ml_de_lubricante
        self.semaforo = threading.Semaphore(1)
        self.lubricante_disponible = True
        self.error = False
        self.tiene_piezas_para_ensamblar = False
        self.cv = threading.Condition()
        self.estado = "Ensamblando"

    def ensamblar_pieza(self):
        self.tiene_piezas_para_ensamblar = True

    def tiene_espacio(self):
        return self.cantidad_piezas_hechas < self.capacidad

    def detener(self):
        self.semaforo.acquire()
        with self.cv:
            self.cv.notify()

    def reanudar(self):
        self.semaforo.release()
        self.error = False

    def verificar_lubricante(self):
        return self.ml_de_lubricante >= 5

    def reponer_lubricante(self):
        self.ml_de_lubricante = 50
        self.error = False
        self.lubricante_disponible = True
        self.reanudar()

    def encender_linea(self, estacion_ensamble):
        while True:
            verificar_lubricante = estacion_ensamble.verificar_lubricante()
            tiene_espacio = estacion_ensamble.tiene_espacio()
            tiene_piezas_para_ensamblar = self.tiene_piezas_para_ensamblar
            no_hay_error = not self.error
            if verificar_lubricante and tiene_piezas_para_ensamblar and tiene_espacio and no_hay_error:
                self.estado = "Ensamblando"
                if random.uniform(0, 1) < 0.15:
                    self.error = True
                else:
                    time.sleep(1)
                    self.cantidad_piezas_hechas += 1
                    self.ml_de_lubricante -= 5
                    self.tiene_piezas_para_ensamblar = False
                    self.pieza_lista_para_soldar()
            elif not verificar_lubricante:
                self.estado = "Sin lubricante"
                self.error = True
                self.lubricante_disponible = False
                estacion_ensamble.detener()
            elif not tiene_piezas_para_ensamblar:
                self.estado = "Sin piezas para ensamblar"
            elif not tiene_espacio:
                self.estado = "Capacidad máxima alcanzada"
                self.pieza_lista_para_soldar()
            else:
                self.estado = "Hay un error en la línea"
                self.error = True
                estacion_ensamble.detener()

    def pieza_lista_para_soldar(self):
        if not self.estacion_soldadura.tiene_piezas_para_soldar:
            self.estacion_soldadura.soldar_pieza()
            self.cantidad_piezas_hechas -= 1
