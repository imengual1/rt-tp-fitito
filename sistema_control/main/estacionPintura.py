import threading
import random
import time


class EstacionPintura:
    def __init__(self, ml_de_pintura, tolerancia_color):
        self.cantidad_piezas_hechas = 0
        self.ml_de_pintura = ml_de_pintura
        self.tolerancia_color = tolerancia_color
        self.semaforo = threading.Semaphore(1)
        self.pintura_disponible = True
        self.error = False
        self.tiene_piezas_para_pintar = False
        self.cv = threading.Condition()
        self.estado = "Pintando"

    def pintar_pieza(self):
        self.tiene_piezas_para_pintar = True

    def detener(self):
        self.semaforo.acquire()
        with self.cv:
            self.cv.notify()

    def reanudar(self):
        self.semaforo.release()
        self.error = False

    def verificar_pintura(self):
        return self.ml_de_pintura >= 50

    def reponer_pintura(self):
        self.ml_de_pintura = 500
        self.error = False
        self.pintura_disponible = True
        self.reanudar()

    def encender_linea(self, estacion_pintura):
        while True:
            verificar_pintura = estacion_pintura.verificar_pintura()
            tiene_piezas_para_pintar = self.tiene_piezas_para_pintar
            no_hay_error = not self.error
            if verificar_pintura and tiene_piezas_para_pintar and no_hay_error:
                self.estado = "Pintando"
                if random.uniform(0, 1) < 0.01:
                    self.error = True
                else:
                    time.sleep(1)
                    self.cantidad_piezas_hechas += 1
                    self.ml_de_pintura -= 50
                    self.tiene_piezas_para_pintar = False
            elif not verificar_pintura:
                self.estado = "Sin pintura"
                self.error = True
                self.pintura_disponible = False
                estacion_pintura.detener()
            elif not tiene_piezas_para_pintar:
                self.estado = "Sin piezas para pintar"
            else:
                self.estado = "Hay un error en la línea"
                self.error = True
                estacion_pintura.detener()
