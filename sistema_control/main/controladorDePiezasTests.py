import unittest
from estacionSoldadura import EstacionSoldadura
from estacionPintura import EstacionPintura
from estacionEnsamble import EstacionEnsamble
from controladorDePiezas import ControladorDePiezas
from unittest.mock import patch

class TestEstacionPintura(unittest.TestCase):
    def setUp(self):
        # Preparacion de tests
        self.estacion_pintura = EstacionPintura(300, 5)
        self.estacion_soldadura = EstacionSoldadura(300, self.estacion_pintura)
        self.estacion_ensamble = EstacionEnsamble(300, self.estacion_soldadura)
        self.controlador_de_piezas = ControladorDePiezas(10, self.estacion_ensamble)
    
    def test_tiene_piezas_para_controlar(self):
        self.assertTrue(self.controlador_de_piezas.tiene_piezas_para_controlar())
   
    def test_tiene_espacio(self):
        # Verificar que la estación informa correctamente sobre el espacio disponible
        self.assertTrue(self.controlador_de_piezas.tiene_espacio())
    
    def test_pieza_lista_para_ensamblar(self):
        self.controlador_de_piezas.tiene_piezas_para_ensamblar = True
        self.controlador_de_piezas.cantidad_de_piezas = 2
        self.controlador_de_piezas.pieza_lista_para_ensamblar()
        self.assertEqual(self.controlador_de_piezas.cantidad_de_piezas , 1)
    
    def test_reanudar(self):
        # Verificar que reanudar cambia el estado de error a False
        self.controlador_de_piezas.error = True
        self.controlador_de_piezas.reanudar()
        self.assertFalse(self.controlador_de_piezas.error)
    
    def test_encender_linea(self):
        # Verificar que la estación informa correctamente sobre el estado de la línea
        with patch.object(ControladorDePiezas, 'encender_linea', return_value=None):
            self.controlador_de_piezas.encender_linea(self.controlador_de_piezas)
            self.assertEqual(self.controlador_de_piezas.estado, "Controlando")
    
if __name__ == '__main__':
    unittest.main()