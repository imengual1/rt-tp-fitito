import unittest
from estacionPintura import EstacionPintura
from unittest.mock import patch

class TestEstacionPintura(unittest.TestCase):
    def setUp(self):
        # Se ejecuta antes de cada test
        self.estacion_pintura = EstacionPintura(300, 5)

    def test_reponer_pintura(self):
        # Verificar que reponer_pintura aumenta la cantidad de pintura disponible
        initial_paint = self.estacion_pintura.ml_de_pintura
        self.estacion_pintura.reponer_pintura()
        self.assertGreater(self.estacion_pintura.ml_de_pintura, initial_paint)

    def test_verificar_pintura_incorrecto(self):
        self.estacion_pintura.ml_de_pintura = 0
        self.assertFalse(self.estacion_pintura.verificar_pintura())

    def test_verificar_pintura_correcto(self):
        # Verificar que la estación informa correctamente sobre la disponibilidad de pintura
        self.assertTrue(self.estacion_pintura.verificar_pintura())

    def test_pintar_pieza(self):
        self.estacion_pintura.pintar_pieza()
        self.assertTrue(self.estacion_pintura.tiene_piezas_para_pintar)

    def test_reanudar(self):
        # Verificar que reanudar cambia el estado de error a False
        self.estacion_pintura.error = True
        self.estacion_pintura.reanudar()
        self.assertFalse(self.estacion_pintura.error)

    
    def test_encender_linea(self):
        # Verificar que la estación informa correctamente sobre el estado de la línea
        with patch.object(EstacionPintura, 'encender_linea', return_value=None):
            self.estacion_pintura.encender_linea(self.estacion_pintura)
            self.assertEqual(self.estacion_pintura.estado, "Pintando")

        
if __name__ == '__main__':
    unittest.main()