import threading
import random
import time


class ControladorDePiezas:
    def __init__(self, cantidad_de_piezas, estacion_ensamble):
        self.estacion_ensamble = estacion_ensamble
        self.cantidad_piezas_controladas = 0
        self.capacidad = 5
        self.cantidad_de_piezas = cantidad_de_piezas
        self.semaforo = threading.Semaphore(1)
        self.error = False
        self.cv = threading.Condition()
        self.estado = "Controlando"

    def tiene_piezas_para_controlar(self):
        return self.cantidad_de_piezas > 0

    def tiene_espacio(self):
        return self.cantidad_piezas_controladas < self.capacidad

    def detener(self):
        self.semaforo.acquire()
        with self.cv:
            self.cv.notify()

    def reanudar(self):
        self.semaforo.release()
        self.error = False

    def encender_linea(self, controlador_de_piezas):
        while True:
            tiene_piezas_para_controlar = self.tiene_piezas_para_controlar()
            tiene_espacio = controlador_de_piezas.tiene_espacio()
            no_hay_error = not self.error
            if tiene_piezas_para_controlar and tiene_espacio and no_hay_error:
                self.estado = "Controlando"
                if random.uniform(0, 1) < 0.20:
                    self.error = True
                else:
                    time.sleep(1)
                    self.cantidad_piezas_controladas += 1
                    self.pieza_lista_para_ensamblar()
            elif not tiene_piezas_para_controlar:
                self.estado = "Terminado"
            elif not tiene_espacio:
                self.estado = "Capacidad máxima alcanzada"
                self.pieza_lista_para_ensamblar()
            else:
                self.estado = "Hay un error en la línea"
                self.error = True
                controlador_de_piezas.detener()

    def pieza_lista_para_ensamblar(self):
        if not self.estacion_ensamble.tiene_piezas_para_ensamblar:
            self.estacion_ensamble.ensamblar_pieza()
            self.cantidad_de_piezas -= 1