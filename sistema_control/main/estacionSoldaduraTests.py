import unittest
from estacionSoldadura import EstacionSoldadura
from estacionPintura import EstacionPintura
from unittest.mock import patch

class TestEstacionPintura(unittest.TestCase):
    def setUp(self):
        # Preparacion de tests
        self.estacion_pintura = EstacionPintura(300, 5)
        self.estacion_soldadura = EstacionSoldadura(300, self.estacion_pintura)
    
    def test_reponer_material(self):
        # Verificar que reponer_material aumenta la cantidad de material disponible
        initial_material = self.estacion_soldadura.gr_de_material
        self.estacion_soldadura.reponer_material()
        self.assertGreater(self.estacion_soldadura.gr_de_material, initial_material)
    
    def test_verificar_material_incorrecto(self):
        self.estacion_soldadura.gr_de_material = 0
        self.assertFalse(self.estacion_soldadura.verificar_material())
    
    def test_verificar_material_correcto(self):
        # Verificar que la estación informa correctamente sobre la disponibilidad de material
        self.assertTrue(self.estacion_soldadura.verificar_material())
    
    def test_soldar_pieza(self):
        self.estacion_soldadura.soldar_pieza()
        self.assertTrue(self.estacion_soldadura.tiene_piezas_para_soldar)
    
    def test_tiene_espacio(self):
        # Verificar que la estación informa correctamente sobre el espacio disponible
        self.assertTrue(self.estacion_soldadura.tiene_espacio())
    
    def test_reanudar(self):
        # Verificar que reanudar cambia el estado de error a False
        self.estacion_soldadura.error = True
        self.estacion_soldadura.reanudar()
        self.assertFalse(self.estacion_soldadura.error)
    
    def test_encender_linea(self):
        # Verificar que la estación informa correctamente sobre el estado de la línea
        with patch.object(EstacionSoldadura, 'encender_linea', return_value=None):
            self.estacion_soldadura.encender_linea(self.estacion_soldadura)
            self.assertEqual(self.estacion_soldadura.estado, "Soldando")
            
if __name__ == '__main__':
    unittest.main()
