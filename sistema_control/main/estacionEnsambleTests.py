import unittest
from estacionSoldadura import EstacionSoldadura
from estacionPintura import EstacionPintura
from estacionEnsamble import EstacionEnsamble
from unittest.mock import patch

class TestEstacionPintura(unittest.TestCase):
    def setUp(self):
        # Preparacion de tests
        self.estacion_pintura = EstacionPintura(300, 5)
        self.estacion_soldadura = EstacionSoldadura(300, self.estacion_pintura)
        self.estacion_ensamble = EstacionEnsamble(20, self.estacion_soldadura)
    
    def test_ensamblar_pieza(self):
        self.estacion_ensamble.ensamblar_pieza()
        self.assertTrue(self.estacion_ensamble.tiene_piezas_para_ensamblar)
    
    def test_tiene_espacio(self):
        # Verificar que la estación informa correctamente sobre el espacio disponible
        self.assertTrue(self.estacion_ensamble.tiene_espacio())

    def test_reponer_lubricante(self):
        # Verificar que reponer_lubricante aumenta la cantidad de lubricante disponible
        initial_lubricante = self.estacion_ensamble.ml_de_lubricante
        self.estacion_ensamble.reponer_lubricante()
        self.assertGreater(self.estacion_ensamble.ml_de_lubricante, initial_lubricante)
    
    def test_verificar_lubricante_incorrecto(self):
        self.estacion_ensamble.ml_de_lubricante = 0
        self.assertFalse(self.estacion_ensamble.verificar_lubricante())
    
    def test_verificar_lubricante_correcto(self):
        # Verificar que la estación informa correctamente sobre la disponibilidad de lubricante
        self.assertTrue(self.estacion_ensamble.verificar_lubricante())

    def test_reanudar(self):
        # Verificar que reanudar cambia el estado de error a False
        self.estacion_ensamble.error = True
        self.estacion_ensamble.reanudar()
        self.assertFalse(self.estacion_ensamble.error)

    def test_encender_linea(self):
        # Verificar que la estación informa correctamente sobre el estado de la línea
        with patch.object(EstacionEnsamble, 'encender_linea', return_value=None):
            self.estacion_ensamble.encender_linea(self.estacion_ensamble)
            self.assertEqual(self.estacion_ensamble.estado, "Ensamblando")   
            
if __name__ == '__main__':
    unittest.main()
