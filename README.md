# Rt Tp Fitito

## Trabajo para la materia Sistemas de Tiempo Real de la carrera de Ingeniería en Computación de la Universidad Nacional de Tres de Febrero

## Integrantes
   - Mariano Medan
   - Ivan Mengual    

## Introducción

Se desarrolla un sistema en tiempo real para una linea de ensamblaje automatizada para una fábrica:

El sistema debe ser capaz de detectar los defectos de las piezas, y en caso de que se detecte un defecto, debe ser capaz de detener la linea de ensamblaje. 
También debe ser capaz de detectar fallas en la linea de ensamblaje, como puede ser la falta de piezas, o linea de ensamblaje detenida.

## Desarrollo

El programa que lo comprende controla 4 estaciones individuales, de control de piezas, ensamblado, soldadura y pintura, conectadas entre sí, modificando piezas de manera secuencial hasta lograr el producto final

Se desarrolla lenguaje Python con la librería threading para ejecutar el programa en diversos hilos. Pensado de tal manera que cada hilo maneje una estación, para poder atender de forma rápida y precisa sus pedidos de manera individual

Las estaciones tienen varios estados:

 - Operando normalmente, indicado en verde en la UI, en este caso procesarán piezas cada un periodo de tiempo fijo y se trasladarán a la siguiente estación

 - Esperando una acción de otra estación, como puede ser entrega de piezas de la estación anterior o liberación de espacio para la estación siguiente. Indicado en amarillo,  este estado será resuelto automáticamente cuando el proceso de su estación necesario se complete.

 - Estado de error, indicado en rojo y esperando la intervención del usuario, para representar esto se utilizan botones en pantalla.


Cada estacion corre un ciclo while true, que se ejecuta constantemente, y en cada iteración se verifica el estado de la estación, y se ejecuta la acción correspondiente a dicho estado. Para que no se ejecute de manera continua, se utiliza la función sleep de la librería time, para que cada ciclo se ejecute cada 0.1 segundos.

A continuación se detalla el funcionamiento de cada estación:

# Estación de control de piezas

La estación de control de piezas recibe las piezas a fabricar y las envía a la siguiente estación.

# Estación de ensamblado

La estación de ensamblado recibe piezas de la estación anterior y las ensambla. En caso de quedarse sin piezas o sin lubricante, se detiene la línea de ensamblaje y se muestra el mensaje de aviso correspondiente en la interfaz gráfica.

# Estación de soldadura

La estación de soldadura recibe piezas de la estación anterior y las suelda. En caso de quedarse sin piezas o material, se detiene la línea y se muestra el mensaje de aviso correspondiente en la interfaz gráfica.

# Estación de pintura

La estación de pintura recibe piezas de la estación anterior y las pinta. En caso de quedarse sin piezas o pintura, se detiene la línea y se muestra el mensaje de aviso correspondiente en la interfaz gráfica.

Para simular errores en la linea de todas las estaciones, se utiliza la librería random, y se genera un número aleatorio entre 0 y 1, y si el número es menor a un umbral, como por ejemplo 0.1, se considera que ocurrió un error en la estación.


## Interfaz gráfica

Para la interfaz gráfica se utiliza la librería tkinter, donde se pueden ver la cantidad de piezas que tiene actualmente la estación, el estado de las estaciones, a través de distintos colores, y los botones para resolver los errores.

Al estar en un estado de error se habilitará el botón correspondiente a la acción que resuelva el error generado y así reanudar la línea. Por ejemplo, al no tener el material suficiente se habilitará un botón el cual repondrá dicho material.

## Tests

Se realizan pruebas unitarias de cada estación, para verificar que el funcionamiento de cada una sea el correcto, y que no se produzcan errores en el funcionamiento de la línea. Se utilizan las libreriás unittest y mock para realizar las pruebas.
